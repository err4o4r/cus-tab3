import {ElementRef, TemplateRef, ViewContainerRef} from "@angular/core";

export interface ICollapse {
  open: ICusTab | null;
  close: ICusTab | null;
}


export interface ICusContentTab {
  id: string
  viewContainerRef : ViewContainerRef;
  templateRef: TemplateRef<any>;
}

export interface ICusHeaderTab {
  id: string
  elementRef: ElementRef;
  isActive : boolean;
}

export interface ICusTab {
  id: string
  viewContainerRef : ViewContainerRef;
  templateRef: TemplateRef<any>;
  elementRef: ElementRef;
  isActive : boolean;
}
