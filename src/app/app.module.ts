import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CusTabDirective } from './cus-tab.directive';
import { CusHeaderTabDirective } from './cus-header-tab.directive';
import { CusContentTabDirective } from './cus-content-tab.directive';

@NgModule({
  declarations: [
    AppComponent,
    CusTabDirective,
    CusHeaderTabDirective,
    CusContentTabDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
